package fr.asalmi.photos.database.repository

import androidx.lifecycle.LiveData
import fr.asalmi.photos.database.Photo
import fr.asalmi.photos.database.PhotoDao

class PhotoRepository(private var photoDao: PhotoDao) {

    fun insert(photos: List<Photo>) {
        return photoDao.insert(photos)
    }

    fun findByAlbumId(albumId: String): LiveData<List<Photo>> {
        return photoDao.findByAlbumId(albumId)
    }
}