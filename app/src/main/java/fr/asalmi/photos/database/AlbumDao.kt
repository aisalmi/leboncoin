package fr.asalmi.photos.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction

@Dao
interface AlbumDao {
    @Transaction
    @Query("SELECT * FROM Album")
    fun find(): List<AlbumPhoto>

    @Insert(onConflict = REPLACE)
    fun insert(albums: List<Album>)
}

@Dao
interface PhotoDao {
    @Insert(onConflict = REPLACE)
    fun insert(photos: List<Photo>)

    @Query("SELECT * FROM Photo WHERE albumId =:albumId")
    fun findByAlbumId(albumId: String): LiveData<List<Photo>>
}