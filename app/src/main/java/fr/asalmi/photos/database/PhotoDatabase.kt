package fr.asalmi.photos.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import fr.asalmi.photos.BuildConfig

@Database(
    entities = [Photo::class, Album::class],
    version = BuildConfig.VERSION_CODE,
    exportSchema = false
)
abstract class PhotoDatabase : RoomDatabase() {
    abstract fun albumPhotoDao(): AlbumDao
    abstract fun photoDao(): PhotoDao

    companion object {
        private const val NAME = "PhotoDB"

        @Volatile
        private var INSTANCE: PhotoDatabase? = null

        fun getInstance(context: Context): PhotoDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, PhotoDatabase::class.java, NAME)
                .fallbackToDestructiveMigration()
                .build()
    }
}