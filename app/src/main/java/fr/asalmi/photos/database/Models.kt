package fr.asalmi.photos.database

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import com.google.gson.annotations.SerializedName

@Entity
data class Album(
    @SerializedName("albumId") @PrimaryKey var id: Long
)

@Entity
data class Photo(
    @PrimaryKey var id: Long,
    var title: String,
    var url: String,
    var thumbnailUrl: String,
    var albumId: String
)

data class AlbumPhoto(
    @Embedded val album: Album,
    @Relation(parentColumn = "id", entityColumn = "albumId") val photos: List<Photo>
)