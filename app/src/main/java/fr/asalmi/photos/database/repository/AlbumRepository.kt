package fr.asalmi.photos.database.repository

import fr.asalmi.photos.database.Album
import fr.asalmi.photos.database.AlbumDao
import fr.asalmi.photos.database.AlbumPhoto

class AlbumRepository(private var albumDao: AlbumDao) {

    fun findAlbumPhoto(): List<AlbumPhoto> {
        return albumDao.find()
    }

    fun insert(albums: List<Album>) {
        return albumDao.insert(albums)
    }
}