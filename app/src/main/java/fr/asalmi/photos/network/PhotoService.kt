package fr.asalmi.photos.network

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import fr.asalmi.photos.database.Album
import fr.asalmi.photos.database.Photo
import java.io.IOException
import java.net.URL

object PhotoService {
    private const val URL = "https://static.leboncoin.fr/img/shared/technical-test.json"
    private val TAG = PhotoService::class.java.simpleName
    private val gson = Gson()
    private val photoType = object : TypeToken<List<Photo>>() {}.type
    private val albumType = object : TypeToken<List<Album>>() {}.type

    fun fetchData(): String? {
        try {
            val url = URL(URL)
            val urlConnection = url.openConnection()
            urlConnection.connectTimeout = 10000
            urlConnection.readTimeout = 10000
            urlConnection.connect()

            return urlConnection.getInputStream().bufferedReader().use { it.readText() }
        } catch (e: Exception) {
            Log.e(TAG, "Error occured while fetching album data from $URL", e)
        }

        return null
    }

    fun parsePhotos(content: String): List<Photo> {
        try {
            return gson.fromJson(content, photoType)
        } catch (e: IOException) {
            Log.e(TAG, "Failed while parsing json", e)
        }

        return listOf()
    }

    fun parseAlbums(content: String): List<Album> {
        try {
            return gson.fromJson(content, albumType)
        } catch (e: IOException) {
            Log.e(TAG, "Failed while parsing json", e)
        }

        return listOf()
    }
}
