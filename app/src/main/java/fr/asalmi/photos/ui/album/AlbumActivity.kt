package fr.asalmi.photos.ui.album

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.asalmi.photos.databinding.ActivityAlbumBinding
import fr.asalmi.photos.helpers.Utils

class AlbumActivity : AppCompatActivity() {
    private lateinit var albumPhotoViewModel: AlbumViewModel
    private lateinit var binding: ActivityAlbumBinding

    private val albumPhotoAdapter = AlbumAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        albumPhotoViewModel = ViewModelProvider(this).get(AlbumViewModel::class.java)
        binding = ActivityAlbumBinding.inflate(layoutInflater)

        binding.albumRecycler.apply {
            layoutManager = GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)
            adapter = albumPhotoAdapter
        }

        albumPhotoViewModel.albumPhoto.observe(this, { albumPhoto ->
            albumPhotoAdapter.setData(albumPhoto)
            if (albumPhoto.isEmpty())
                checkInternet()
        })

        setContentView(binding.root)
    }

    private fun checkInternet() {
        if (!Utils.isConnected(this)) {
            binding.albumRecycler.visibility = View.INVISIBLE
            binding.infos.visibility = View.VISIBLE
        } else {
            binding.albumRecycler.visibility = View.VISIBLE
            binding.infos.visibility = View.INVISIBLE
        }
    }

    companion object {
        private val TAG = AlbumActivity::class.java.simpleName
    }
}