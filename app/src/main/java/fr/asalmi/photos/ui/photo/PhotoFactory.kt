package fr.asalmi.photos.ui.photo

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PhotoFactory(private val application: Application, private val albumId: String) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PhotoViewModel::class.java)) {
            return PhotoViewModel(application, albumId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}