package fr.asalmi.photos.ui.album

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import fr.asalmi.photos.database.AlbumPhoto
import fr.asalmi.photos.database.PhotoDatabase
import fr.asalmi.photos.database.repository.AlbumRepository
import fr.asalmi.photos.database.repository.PhotoRepository
import fr.asalmi.photos.helpers.Utils
import fr.asalmi.photos.network.PhotoService
import java.util.concurrent.Executors

class AlbumViewModel(application: Application) : AndroidViewModel(application) {
    private val albumRepository =
        AlbumRepository(PhotoDatabase.getInstance(application).albumPhotoDao())
    private val photoRepository =
        PhotoRepository(PhotoDatabase.getInstance(application).photoDao())

    private val executor = Executors.newSingleThreadExecutor()

    var albumPhoto: MutableLiveData<List<AlbumPhoto>> = MutableLiveData()

    init {
        getAlbum()
    }

    private fun getAlbum() {
        executor.execute {
            if (!Utils.isConnected(getApplication())) {
                albumPhoto.postValue(albumRepository.findAlbumPhoto())
            } else {
                val data = PhotoService.fetchData()

                data?.let {
                    val photos = PhotoService.parsePhotos(data)
                    val albums = PhotoService.parseAlbums(data)

                    photoRepository.insert(photos)
                    albumRepository.insert(albums)
                }

                albumPhoto.postValue(albumRepository.findAlbumPhoto())
            }
        }
    }

    companion object {
        val TAG = AlbumViewModel::class.java.simpleName
    }
}