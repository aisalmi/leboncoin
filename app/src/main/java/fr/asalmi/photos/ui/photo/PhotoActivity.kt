package fr.asalmi.photos.ui.photo

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.asalmi.photos.databinding.ActivityPhotoBinding
import fr.asalmi.photos.helpers.Utils
import fr.asalmi.photos.ui.album.AlbumActivity

class PhotoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPhotoBinding
    private val photoAdapter = PhotoAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val photoViewModel =
            ViewModelProvider(this, PhotoFactory(application, "1")).get(PhotoViewModel::class.java)

        binding = ActivityPhotoBinding.inflate(layoutInflater)

        binding.photoRecycler.apply {
            layoutManager = GridLayoutManager(context, 3, RecyclerView.VERTICAL, false)
            adapter = photoAdapter
        }

        photoViewModel.photos.observe(this, { photos ->
            photoAdapter.setData(photos)

            if (photos.isEmpty())
                checkInternet()
        })

        setContentView(binding.root)
    }

    private fun checkInternet() {
        if (!Utils.isConnected(this)) {
            binding.photoRecycler.visibility = View.INVISIBLE
            binding.infos.visibility = View.VISIBLE
        } else {
            binding.photoRecycler.visibility = View.VISIBLE
            binding.infos.visibility = View.INVISIBLE
        }
    }

    companion object {
        private val TAG = AlbumActivity::class.java.simpleName
        const val EXTRA_ALBUM_ID = "EXTRA_ALBUM_ID"
    }
}