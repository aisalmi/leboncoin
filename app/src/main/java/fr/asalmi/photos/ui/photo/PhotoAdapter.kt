package fr.asalmi.photos.ui.photo

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import fr.asalmi.photos.R
import fr.asalmi.photos.database.Photo
import fr.asalmi.photos.helpers.Utils

class PhotoAdapter : RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {
    private var photos: List<Photo> = listOf()

    class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
    ) {
        val photo: ImageView = itemView.findViewById(R.id.photo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo = photos[position]

        val url = GlideUrl(
            photo.url.trim(),
            LazyHeaders.Builder().addHeader("User-Agent", Utils.USER_AGENT).build()
        )

        Glide.with(holder.itemView)
            .load(url)
            .fitCenter()
            .error(R.drawable.ic_bg_sadcloud)
            .into(holder.photo)
    }

    override fun getItemCount() = photos.size

    fun setData(photos: List<Photo>) {
        this.photos = photos
        notifyDataSetChanged()
    }

    companion object {
        private val TAG = PhotoAdapter::class.java.simpleName
    }
}