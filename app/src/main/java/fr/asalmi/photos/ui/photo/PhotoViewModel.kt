package fr.asalmi.photos.ui.photo

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import fr.asalmi.photos.database.PhotoDatabase
import fr.asalmi.photos.database.repository.PhotoRepository

class PhotoViewModel(application: Application, albumId: String) :
    AndroidViewModel(application) {
    private val photoRepository =
        PhotoRepository(PhotoDatabase.getInstance(application).photoDao())

    var photos = photoRepository.findByAlbumId(albumId)

    companion object {
        val TAG = PhotoViewModel::class.java.simpleName
    }
}