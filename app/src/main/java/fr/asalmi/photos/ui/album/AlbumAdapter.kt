package fr.asalmi.photos.ui.album

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import fr.asalmi.photos.R
import fr.asalmi.photos.database.AlbumPhoto
import fr.asalmi.photos.helpers.Utils
import fr.asalmi.photos.ui.photo.PhotoActivity
import fr.asalmi.photos.ui.photo.PhotoActivity.Companion.EXTRA_ALBUM_ID

class AlbumAdapter : RecyclerView.Adapter<AlbumAdapter.ViewHolder>() {
    private var albumPhotos: List<AlbumPhoto> = listOf()

    class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_album, parent, false)
    ) {
        val thumbnail: ImageView = itemView.findViewById(R.id.thumbnail)
        val title: TextView = itemView.findViewById(R.id.title)
        val photos: TextView = itemView.findViewById(R.id.photos)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val albumPhoto = albumPhotos[position]
        val album = albumPhoto.album
        val lastPhotoThumbnailUrl = albumPhoto.photos.last().thumbnailUrl
        val context = holder.itemView.context

        holder.title.text = album.id.toString()
        holder.photos.text = albumPhoto.photos.size.toString()

        val url = GlideUrl(
            lastPhotoThumbnailUrl.trim(),
            LazyHeaders.Builder().addHeader("User-Agent", Utils.USER_AGENT).build()
        )

        Glide.with(holder.itemView)
            .load(url)
            .fitCenter()
            .error(R.drawable.ic_bg_sadcloud)
            .into(holder.thumbnail)

        holder.itemView.setOnClickListener {
            val intent = Intent(context, PhotoActivity::class.java).apply {
                putExtra(EXTRA_ALBUM_ID, album.id)
            }
            context.startActivity(intent)
        }
    }

    override fun getItemCount() = albumPhotos.size

    fun setData(albumPhotos: List<AlbumPhoto>) {
        this.albumPhotos = albumPhotos
        notifyDataSetChanged()
    }

    companion object {
        private val TAG = AlbumAdapter::class.java.simpleName
    }
}